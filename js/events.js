var rating = document.getElementById("btn-rating");
var oldest = document.getElementById("btn-oldest");
var newest = document.getElementById("btn-newest");
var search = document.getElementById("input-search");
var asc		= 1;
var desc 	= 0;

function toggleActive(el){
	var active = document.getElementsByClassName("btn-order-active")[0];
	if(active!=undefined){
		active.classList.remove("btn-order-active");
	}
	if(el!=undefined){
		el.classList.add("btn-order-active");
	}
}

//Adding Event Listeners to the buttons
rating.addEventListener("click", function(){
    toggleActive(this);
	populate(orderType[1],desc);
});

oldest.addEventListener("click", function(){
    toggleActive(this);
    populate(orderType[0],asc);
});

newest.addEventListener("click", function(){
    toggleActive(this);
    populate(orderType[0],desc);
});
search.addEventListener("keydown", function(){
	toggleActive();
	var keyword = this;
	setTimeout(function(){populate(orderType[2],keyword.value)},100);
});