var path1 	= '/assets/source1.json';
var path2 	= '/assets/source2.json';
var asc		= 1;
var desc 	= 0;
var keyword = '';
var orderType 	= ['date', 'rating', 'keyword'];
//ajax reload time
var ajaxReload = 20000;
var localStorageNameJSON = "items";

//AJAX request function
function ajax(path, callback) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                var data = httpRequest.responseText;
	    		var json = data.replace(/'/g , '"');
                if (callback) callback(json);
            }
        }
    };
    httpRequest.open('GET', path);
    httpRequest.send(); 
}

//Initialise JSONProcessor object
function jsonProcessor() {};

jsonProcessor.prototype = {
	date: function(array,direction) { 

	    array.sort(function (a, b) {
		    return a.posttime.localeCompare(b.posttime);
		});

		if(direction){
			array = array;
		}else{
			array = array.reverse();
		}

		return array

	},
	rating: function(array,direction) { 

		array.sort(function(a, b){
			return a.rating - b.rating
		});

		if(direction){
			array = array;
		}else{
			array = array.reverse();

		}

		return array
	},
	keyword: function(array,keyword) { 

	    var deleteRows = new Array();

		for(var i = 0; i < array.length; i++) {

	    	var found = 0;
		    var obj 	= array[i];

			for(var j = 0; j < array[i].tags.length; j++) {

		   		var obj2 = array[i].tags[j];
		   		if(!found){
		    		found = obj2.indexOf(keyword) == -1 ? 0 : 1;
		   		}

			}

		   	if(!found){
		    	found = obj.title.indexOf(keyword) == -1 ? 0 : 1;
		    }

		    if(found){
		    	deleteRows.push(i)
		    }
		}

		for(var i = deleteRows.length; i-- > 0; ) {
			array.splice(deleteRows[i],1)
		}

		return array
   }
};


//The function which is called to bring the data
function populate(orderType, variable){
    this.jP 	= new jsonProcessor();
	var array 	= [];
	var array1 	= [];
	var array2 	= [];


	//manage request
	function manageReq(array,variable){

			//Depending on the order type requested process the json array

			//Process the array in terms of date
		    if (orderType == 'date'){
		    	//Emptying the input search box
				document.getElementById("input-search").value = '';

			    array = this.jP.date(array,variable);
			
				htmlPopulate(array);

		    }

			//Process the array in terms of rating
		    if (orderType == 'rating'){
		    	//Emptying the input search box
				document.getElementById("input-search").value = '';

			    array = this.jP.rating(array,variable);
				htmlPopulate(array);
		    }


			//Process the array in terms of keyword
		    if (orderType == 'keyword'){
			    array = this.jP.keyword(array,variable);
			    hideItems(array);
		    }

	};


	if (localStorage.getItem(localStorageNameJSON) === null) {
		//First ajax Request
		ajax(path1, function(data1){

			//Try converting the first ajax response into json
		    try {
				array1 = JSON.parse(data1);
		    } catch(e) {
		        alert(e); 
		    }

		    //Second ajax request
			ajax(path2, function(data2){

				//Try converting the Second ajax response into json
				try {
					array2 = JSON.parse(data2);
			    } catch(e) {
			        alert(e); 
			    }
				
				//Concat two json arrays into one
				array = array2.concat(array1);

				//manageRequest function 
				manageReq(array,variable);

				//Initialise items JSON
				localStorage.setItem(localStorageNameJSON, JSON.stringify(array));

				//Destroy Item after set miliseconds
				setTimeout(function(){localStorage.removeItem(localStorageNameJSON)},ajaxReload);
			});

		});
	}else{
		const data = JSON.parse(localStorage.getItem(localStorageNameJSON));
		manageReq(data,variable);
	};

}

//Populate with new data
function htmlPopulate(array){
	
	var title,rating,rating_cont,image,img_cont,tag,tag_cont,date,footer,item,itemHTML;
	var items = document.getElementById("items");
	items.innerHTML = ''
	for(var i = 0; i < array.length; i++) {

		//Item's title
		title 		= '<h2>'+array[i].title+'</h2>';

		//Item's image container
		rating 		= array[i].rating == undefined ? 'xxx' : array[i].rating;
		rating_cont = '<span class="item-likes float-right"><i class="fa fa-thumbs-up" aria-hidden="true"></i>&nbsp;'+rating+'</span>';
		image 		= '<img class="img-item" src="'+array[i].thumbnail_url+'" alt="'+array[i].title+'"/>';
		img_cont 	= '<a href="'+array[i].video_url+'" class="img-cont">'+rating_cont+image+'</a>';

		//Item's footer container
		tag 		= '';
		for(var j = 0; j < array[i].tags.length; j++) {
			tag += '<span class="item-tag">'+array[i].tags[j]+'</span>'
		}

		date 		= '<span class="float-right item-date">'+array[i].posttime.slice(0, 16).replace(/T/g , ' ')+'</span>';

		tag_cont 	= '<div class="row"><div class="col-6 item-tags">'+tag+'</div><div class="col-6">'+date+'</div></div>';

		//Create Item
		item 		= '<div id="'+array[i].id+'" class="item col-4 col-md-6 col-xs-12">'+title+img_cont+tag_cont+'</div>';
		
		//Append Item to
		itemHTML = str2DOMElement(item);
		items.appendChild(itemHTML);
	}
}

//Populate with new data
function hideItems(array){
	console.log(array)
	var all = document.getElementsByClassName('item');
	for (var i = 0; i < all.length; i++) {
	  all[i].style.display = 'block';
	}
	for (var i = 0; i < array.length; i++) {
		document.getElementById(array[i].id).style.display = 'none';
	}
}
//Convert string to html
var str2DOMElement = function(html) {
    var frame = document.createElement('iframe');
    frame.style.display = 'none';
    document.body.appendChild(frame);             
    frame.contentDocument.open();
    frame.contentDocument.write(html);
    frame.contentDocument.close();
    var el = frame.contentDocument.body.firstChild;
    document.body.removeChild(frame);
    return el;
}


populate(orderType[0],asc)